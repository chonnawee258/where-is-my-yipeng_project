using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chonnawee.GameDev3.chapter1
{
    public enum ItemType {
     COIN,
     BIGCOIN,
     POWERUP,
     POWERDOWN,
     SUPERPOWERUP,
     SUPERPOWERDOWN,
     }
}