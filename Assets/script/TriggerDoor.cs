using System;
using System.Collections;
using System.Collections.Generic;
using Chonnawee.GameDev3.chapter5;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class TriggerDoor : MonoBehaviour
{
    [SerializeField] 
     GameObject door;

     bool isOpend = false;
    void OnTriggerEnter(Collider col)
    {
        if (col.GameObject().tag == "Box")
        {
            if (!isOpend)
            {
                isOpend = true;
                door.transform.position += new Vector3(0, 7, 0);
            }
        }
    }
}
