using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Polybrush;


namespace Chonnawee.GameDev3.chapter6
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] protected float m_TimerDuration = 5;

        [SerializeField] protected UnityEvent m_TimerStartEvent = new();
        [SerializeField] protected UnityEvent m_TimerEndEvent = new();
        [SerializeField] private TextMeshProUGUI timerText;

        protected bool _IsTimerStart = false;
        protected float _StartTimeStamp;
        protected float _EndTimeStamp;

        [SerializeField] protected float _CurrentTime;

        void Update()
        {
            if (!_IsTimerStart) return;

            _CurrentTime = (Time.time - _StartTimeStamp);

            if (Time.time >= _EndTimeStamp)
            {
                EndTimer();
            }

            if (_CurrentTime > 0)
            {
                _CurrentTime -= Time.deltaTime;
            }

            int minutes = Mathf.FloorToInt(_CurrentTime / 60);
            int seconds = Mathf.FloorToInt(_CurrentTime % 60);
            timerText.text = string.Format("{0:00} : {1:00}", minutes, seconds);
        }

        public virtual void StartTimer()
        {
            //Check if the timer is already running
            if (_IsTimerStart) return;

            m_TimerStartEvent.Invoke();

            _IsTimerStart = true;
            _StartTimeStamp = Time.time;
            _EndTimeStamp = Time.time + m_TimerDuration;

        }

        public virtual void EndTimer()
        {
            m_TimerEndEvent.Invoke();
            _IsTimerStart = false;
        }

    }
}
